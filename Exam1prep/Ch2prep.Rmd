---
title: "Ch2prep"
author: "Nesrine Naaman"
date: "11/24/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
setwd("C:/Users/callertech/Documents/Uni/Fall_2020/Data Mining/bif524_fall20_naaman_nesrine")

```

PACKAGES REQUIRED:

-stats
-boot
-class
-car
-dplyr
-base
-openxlsx
-olsrr
-MASS

## Ch2:

-rnorm(nb of obs)
-set.seed(any nb)
-median(vector)
-mean(vector)
-sd(vector)
-var(vector)
-cor(vector1,vector2)
-plot(x,y)
-pdf("figure1.pdf")
-dev.off()
-seq(1,10, length=10, by=0.5)

-read.table(Auto.data, header=TRUE, na.strins="?")

-fix(Auto)
-attach(Auto)

-read.csv("Auto.csv")

-na.omit(Auto)
-as.factor(cylinders)
-hist(x)
-summary(Auto)
-pairs(~., dataset) DOESNT WORK IF A PREDICTOR IS NON NUMERIC
-pairs(~ mpg+displacement+cylinders, Auto)
-identify(x,y, third column ex name) WORKS AFTER THE FUNCTION PLOT

-a data frame storing rows that have rad>7: conditions in data frames
rad7=Boston[Boston[,"rad"]>7,]

-min(vector)
-max(vector)
-range(vector)
-table(qualitative variable)
