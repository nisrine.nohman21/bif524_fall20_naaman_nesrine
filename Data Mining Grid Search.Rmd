---
title: "Data Mining Grid Search"
author: "Nesrine Naaman"
date: "9/30/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
```{r}

setwd("C:/Users/callertech/Documents/Uni/Fall_2020/Data Mining/bif524_fall20_naaman_nesrine")

```

## GridSearch function

A function that takes one argument: the price you're willing to provide for both the TV and radio ads in dollars. The function computes the optimal way you can divide the budget on TV and radio in order to get the maximal sales possible. It prints the optimal price to spend on TV, then the optimal one to spend on radio and finally the maximum sales it can give us.

```{r}

GridSearch = function(a)
{   TV=0
    rd=0
    max=0
    BestTV=TV
    Bestrd=rd
    
    for(i in 1:a)
    {
      TV=i
      rd=a-i
      sales=6.7502+0.0191*TV+0.0289*rd+0.0011*rd*TV
      if(sales>max)
      {
        max=sales
        BestTV=TV
        Bestrd=rd
      }
      
    }
  print(BestTV)
  print(Bestrd)
  print(max)
}


GridSearch(5000)



```
